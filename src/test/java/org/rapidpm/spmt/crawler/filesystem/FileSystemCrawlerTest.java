package org.rapidpm.spmt.crawler.filesystem;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.rapidpm.commons.cdi.format.CDISimpleDateFormatter;
import org.rapidpm.commons.cdi.logger.CDILogger;
import org.rapidpm.commons.cdi.se.CDICommonsSE;
import org.rapidpm.commons.cdi.se.CDICommonsSEQualifier;
import org.rapidpm.commons.cdi.se.CDIContainerSingleton;
import org.rapidpm.commons.cdi.se.filesystem.TargetDateFile;
import org.rapidpm.module.se.commons.logger.Logger;
import org.rapidpm.spmt.crawler.Crawler;
import org.rapidpm.spmt.crawler.CrawlerController;
import org.rapidpm.spmt.data.Node;

import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;

import static org.rapidpm.spmt.crawler.filesystem.extractor.FileNameExtractor.FILE_NAME;
import static org.rapidpm.spmt.crawler.filesystem.extractor.FileNameExtractor.FILE_PATH;

/**
 * FileSystemCrawler Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>Mai 21, 2013</pre>
 */
public class FileSystemCrawlerTest  {

    public static final String SOURCE_DRIVE_NAME = "G:\\";
    public static final String TARGET_FOLDER_WORKED_ON = SOURCE_DRIVE_NAME + "workedon";
    public static final String TARGET_FOLDER_BASE_DIR = "E:\\PhotosOpa\\export";


    private final CDIContainerSingleton cdi = CDIContainerSingleton.getInstance();
    private final CrawlerController crawlerController = cdi.getManagedInstance(CrawlerController.class);
    private final Logger logger = cdi.getManagedInstance(Logger.class);
//    private final TargetDateFile targetDateFile = cdi.getManagedInstance(new CDICommonsSEQualifier() {}, TargetDateFile.class);
    private final TargetDateFile targetDateFile = new TargetDateFile();
//    private final TargetDateFile targetDateFile = cdi.getManagedInstance(TargetDateFile.class);

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {
    }

    /*
    Image Height
    Image Width / Exif Image Width
    Exposure Time - belichtungszeit
    F-Number - blende
    ISO Speed Ratings -
    Date/Time Original
    Focal Length
    Make - Marke
    Model

    '/
    /**
     * Method: crawl(final Node rootNode, final String uri)
     */
    @Test
    public void testCrawl() throws Exception {
        final File exportDir = new File(TARGET_FOLDER_BASE_DIR);

        final Crawler<String> crawler = crawlerController.getFsCrawler();
        final Node rootNode = new Node();
//        crawler.addStartURL(SOURCE_DRIVE_NAME + "m.objects Daten");
//        crawler.addStartURL(SOURCE_DRIVE_NAME + "MED PH-VIDEO-u.a. (J)");
//        crawler.addStartURL(SOURCE_DRIVE_NAME + "MEDION");
//        crawler.addStartURL(SOURCE_DRIVE_NAME + "Photos 2");
//        crawler.addStartURL(SOURCE_DRIVE_NAME + "Sticks Aug 2013");
//        crawler.addStartURL(SOURCE_DRIVE_NAME + "Sticks Juni 2013");
//        crawler.addStartURL(SOURCE_DRIVE_NAME + "Sticks Nov 2013");
        crawler.addStartURL(SOURCE_DRIVE_NAME + "Alle W3-Photos, unsortiert");
        crawler.addStartURL(SOURCE_DRIVE_NAME + "Backups");
        crawler.addStartURL(SOURCE_DRIVE_NAME + "m.objects Daten");
        crawler.addStartURL(SOURCE_DRIVE_NAME + "Rettung TowerM Januar2013");
        crawler.addStartURL(SOURCE_DRIVE_NAME + "von Photos2");

        crawler.addRoodNode(rootNode);
        crawler.crawl();

        workOnNodeMove(exportDir, rootNode);

        System.out.println(rootNode);
    }



    private final String datePatterns[] = {
            "EEE MMM dd HH:mm:ss zzzz yyyy",
            "yyyy:MM:dd HH:mm:ss",
            "yyyy:MM:dd HH:mm",
            "yyyy-MM-dd HH:mm:ss",
            "yyyy-MM-dd HH:mm",
            "yyyy.MM.dd HH:mm:ss",
            "yyyy.MM.dd HH:mm"};


    private Date parseDate(final String dateString, TimeZone timeZone) {
        for (String datePattern : datePatterns) {
            try {
                DateFormat parser = new SimpleDateFormat(datePattern);
                if (timeZone != null)
                    parser.setTimeZone(timeZone);
                return parser.parse(dateString);
            } catch (ParseException ex) {
                // simply try the next pattern
            }
        }
        //System.out.println("datePatterns = " + dateString);
        return null;
    }

    private Map<String, HashCode> hashCodeMap = new HashMap<>();

    private void workOnNodeMove( File exportDir, Node rootNode) throws ParseException, IOException {
        if (rootNode.isFile()) {
            workOnFile(exportDir, rootNode);
        } else {
            final List<Node> nodeList = rootNode.getNodeList();
            for (final Node node : nodeList) {
                workOnNodeMove(exportDir, node);
            }
        }

    }

    private void workOnFile(File exportDir, Node rootNode) throws IOException {
        final Map<String, String> attributMap = rootNode.getAttributMap();
        final String fileName = attributMap.get(FILE_NAME);
        final String pathName = attributMap.get(FILE_PATH);

        final File sourceFile = new File(pathName, fileName);
        System.out.println("sourceFile.getName() = " + sourceFile.getName());
        if (attributMap.containsKey("Date/Time Original")) {
            final String value = attributMap.get("Date/Time Original");

            final Date creationTimeDate = parseDate(value, null);
            if (creationTimeDate == null) {
                //uebergehen
                System.out.println("value (creationTimeDate) = " + value);
            } else {


                final File dd = targetDateFile.createDailyDir(exportDir, creationTimeDate);

                final String sourceFileName = sourceFile.getName();
                final File destinationFile = new File(dd, sourceFileName);
                if (destinationFile.exists()) {
                    //Kollisionsbeghandlung
                    //vergleiche MD5 wenn gleich nicht schieben, verschiebe in deleteFolder
                    final HashCode hcSource = com.google.common.io.Files.hash(sourceFile, Hashing.md5());
                    final HashCode hcDestination;

                    if(hashCodeMap.containsKey(destinationFile.toString())){
                        hcDestination = hashCodeMap.get(destinationFile.toString());
                    }else {
                        hcDestination = com.google.common.io.Files.hash(destinationFile, Hashing.md5()); //cachen
                        hashCodeMap.put(destinationFile.toString(), hcDestination);
                    }

                    if (hcDestination.equals(hcSource)) {
                        //gleicher HashWert
                        System.out.println(" MD5 ist eq. = source/desti " + sourceFileName + " -> " + destinationFile.getName());
//                            final File duplicateDir = new File("E:/PhotosOpa/duplicate");
//                            final File ddNew = new TargetDateFile().createDailyDir(duplicateDir, creationTimeDate);
////                            final String[] split = sourceFileName.split(".");
////                            String fileEnding = split[split.length - 1];
//                            String fileEnding = "";
//                            int i = sourceFileName.lastIndexOf('.');
//                            if (i > 0 &&  i < sourceFileName.length() - 1) {
//                                fileEnding = sourceFileName.substring(i+1).toLowerCase();
//                            }
//                            final File destinationFileNew = new File(ddNew, sourceFileName + "_" + System.nanoTime()+"."+fileEnding);
////                            Files.move(sourceFile.toPath(), destinationFileNew.toPath(), StandardCopyOption.REPLACE_EXISTING);
//                            Files.copy(sourceFile.toPath(), destinationFileNew.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    } else {
                        final File destinationFileNew = new File(dd, sourceFileName + "_" + System.nanoTime());
                        Files.copy(sourceFile.toPath(), destinationFileNew.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    }
                } else {
                    Files.copy(sourceFile.toPath(), destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                }
                final Path target = new File(new File(TARGET_FOLDER_WORKED_ON), sourceFile.getPath().replace(SOURCE_DRIVE_NAME, "")).toPath();
                final Path parent = target.getParent();
                parent.toFile().mkdirs();
                Files.move(sourceFile.toPath(), target, StandardCopyOption.REPLACE_EXISTING);
            }

        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("kein Zeitstempel / Aufnahmedatum");
            }
        }
    }


    /**
     * Method: crawlNextLevel(Node rootNode, File startPoint)
     */
   // @Test
    public void testCrawlNextLevel() throws Exception {
//TODO: Test goes here... 
/* 
try { 
   Method method = FileSystemCrawler.getClass().getMethod("crawlNextLevel", Node.class, File.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/
    }




    public static  class TargetDateFile implements Serializable {

        private SimpleDateFormat sdfYYYY = new SimpleDateFormat("yyyy");
        private SimpleDateFormat sdfMM= new SimpleDateFormat("MM");
        private SimpleDateFormat sdfdd= new SimpleDateFormat("dd");;


        public File createDailyDir(File baseDir, Date date) {
            final File yyyy = new File(baseDir, sdfYYYY.format(date));
            final File mm = new File(yyyy, sdfMM.format(date));
            final File dd = new File(mm, sdfdd.format(date));
            if (yyyy.exists()) {
                //
            } else {
                yyyy.mkdir();
            }

            if (mm.exists()) {
            } else {
                mm.mkdir();
            }
            if (dd.exists()) {
            } else {
                dd.mkdir();
            }
            return dd;
        }

    }


} 
