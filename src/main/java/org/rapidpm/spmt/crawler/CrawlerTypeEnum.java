package org.rapidpm.spmt.crawler;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 22.05.13
 * Time: 21:51
 * To change this template use File | Settings | File Templates.
 */
public enum CrawlerTypeEnum {
    FILE_SYSTEM,
    WEB,
    MAIL,
    DB
}
