package org.rapidpm.spmt.crawler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 22.05.13
 * Time: 21:48
 * To change this template use File | Settings | File Templates.
 */


@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE, ElementType.METHOD})
public @interface CrawlerType {
    CrawlerTypeEnum value();
}


