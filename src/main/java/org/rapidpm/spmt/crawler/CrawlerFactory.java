package org.rapidpm.spmt.crawler;

import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;

import org.rapidpm.spmt.crawler.filesystem.FileSystemCrawler;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 22.05.13
 * Time: 21:32
 * To change this template use File | Settings | File Templates.
 */
public class CrawlerFactory {

    /*
    @Produces @CrawlerType(CrawlerTypeEnum.FILE_SYSTEM)
    public Crawler createFileSystemCrawlerInstance(){
        return new FileSystemCrawler();
    }
    */
    @Produces @Default
    public Crawler createDefaultCrawlerInstance(){
        return new FileSystemCrawler();
    }
}
