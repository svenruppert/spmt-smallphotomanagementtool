package org.rapidpm.spmt.crawler;

import javax.validation.constraints.NotNull;

import org.rapidpm.spmt.data.Node;

/**
 * Created with IntelliJ IDEA.
 * User: rapidpm
 * Date: 24.01.13
 * Time: 16:48
 * To change this template use File | Settings | File Templates.
 */
public interface Crawler<T> {

    public void crawl();

    public void addRoodNode(@NotNull final Node rootNode);
    public void addStartURL(@NotNull final T uri);
}
