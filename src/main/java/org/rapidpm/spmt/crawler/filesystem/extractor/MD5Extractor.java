package org.rapidpm.spmt.crawler.filesystem.extractor;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;
import org.rapidpm.commons.cdi.logger.CDILogger;
import org.rapidpm.module.se.commons.logger.Logger;
import org.rapidpm.spmt.data.Node;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 26.05.13
 * Time: 00:30
 * To change this template use File | Settings | File Templates.
 */
public class MD5Extractor implements FileExtractor {

    @Inject @CDILogger
    private Logger logger;


    @Override
    public void extract(File file, Node node) {
        if (file.isFile()) {
            try {
                final HashCode hc = Files.hash(file, Hashing.md5());
                node.getAttributMap().put("MD5", hc.toString());
            } catch (IOException e) {
                logger.error(e);
            }
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("file ist ein Verz. " + file.getName());
            }
        }
    }
}
