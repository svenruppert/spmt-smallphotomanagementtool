package org.rapidpm.spmt.crawler.filesystem;

import org.rapidpm.spmt.crawler.filesystem.extractor.FileExifinfoExtractor;
import org.rapidpm.spmt.crawler.filesystem.extractor.FileExtractor;
import org.rapidpm.spmt.crawler.filesystem.extractor.FileNameExtractor;
import org.rapidpm.spmt.crawler.filesystem.extractor.FileSizeExtractor;
import org.rapidpm.spmt.crawler.filesystem.extractor.MD5Extractor;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.New;
import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: rapidpm
 * Date: 24.01.13
 * Time: 16:31
 * To change this template use File | Settings | File Templates.
 */
public class FileExtractorRegistry {

    @Inject @New private FileSizeExtractor fileSizeExtractor;
    @Inject @New private FileNameExtractor fileNameExtractor;
    @Inject @New private FileExifinfoExtractor fileExifinfoExtractor;
    @Inject @New private MD5Extractor md5Extractor;

    public List<FileExtractor> getExtractorList() {
        final List<FileExtractor> list = new ArrayList<>();
        list.add(fileSizeExtractor); //TODO perAnnotationen ?
        list.add(fileNameExtractor); //TODO perAnnotationen ?
        list.add(fileExifinfoExtractor); //TODO perAnnotationen ?
//        list.add(md5Extractor); //TODO perAnnotationen ?
        return list;
    }
}
