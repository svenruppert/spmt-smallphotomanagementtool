package org.rapidpm.spmt.crawler.filesystem.extractor;


import org.rapidpm.commons.cdi.logger.CDILogger;
import org.rapidpm.spmt.data.Node;

import java.io.File;


import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: rapidpm
 * Date: 24.01.13
 * Time: 16:21
 * To change this template use File | Settings | File Templates.
 */
public class FileSizeExtractor implements FileExtractor {

    @Inject @CDILogger
    private org.rapidpm.module.se.commons.logger.Logger logger;

    @Override
    public void extract(final File file, final Node node){
        final long length = file.length();
        node.getAttributMap().put(FILE_SIZE, length+"");
        if (logger.isDebugEnabled()) {
            logger.debug("extracted length " + length+"");
        }
    }


}
