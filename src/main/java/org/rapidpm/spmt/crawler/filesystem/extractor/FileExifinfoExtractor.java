package org.rapidpm.spmt.crawler.filesystem.extractor;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Set;

import javax.inject.Inject;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.google.common.collect.Sets;
import org.rapidpm.commons.cdi.logger.CDILogger;
import org.rapidpm.spmt.data.Node;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 21.05.13
 * Time: 20:40
 * To change this template use File | Settings | File Templates.
 */
public class FileExifinfoExtractor implements FileExtractor {
    private static final String FILE_PIC_CREATION_DATE = "FILE_PIC_CREATION_DATE";
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");

    @Inject @CDILogger
    private org.rapidpm.module.se.commons.logger.Logger logger;

    private Set<String> exifTagsSet = Sets.newHashSet("Exif Image Height", "Exif Image Width", "Exposure Time", "F-Number",
            "ISO Speed Ratings", "Date/Time Original", "Focal Length", "Make", "Model");

    @Override
    public void extract(File file, Node node) {
        if (file.isDirectory()) {
            if (logger.isDebugEnabled()) {
                logger.debug("kein Exif : Verz. " + file.getName());
            }
        } else {
            try {
                final Metadata metadata = ImageMetadataReader.readMetadata(file);
                final int directoryCount = metadata.getDirectoryCount();
                if (directoryCount > 0) {
                    final Iterable<Directory> metadataDirectories = metadata.getDirectories();
                    for (final Directory metadataDirectory : metadataDirectories) {
                        final Collection<Tag> tags = metadataDirectory.getTags();
                        for (final Tag tag : tags) {
                            final String tagName = tag.getTagName();
                            if (exifTagsSet.contains(tagName)) {
                                final String creationDate = metadataDirectory.getString(tag.getTagType());
                                node.getAttributMap().put(tagName, creationDate);
                                if (logger.isDebugEnabled()) {
                                    logger.debug("extracted " + creationDate);
                                }
                            } else {
                                if (logger.isDebugEnabled()) {
                                    logger.debug("tagName " + tagName);
                                }
                            }
                        }
                        //final ExifSubIFDDirectory directory = metadata.getDirectory(ExifSubIFDDirectory.class);
//                    final Date creationTimeDate = directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL); //Liste von Attributen extrahieren
//                    if (creationTimeDate != null) {
//                    } else {
//                        if (logger.isDebugEnabled()) {
//                            logger.debug(FILE_PIC_CREATION_DATE + " is null");
//                        }
//                    }
                    }
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("directoryCount <= 0");
                    }
                }


            } catch (ImageProcessingException | IOException e) {
                logger.error(e);
            }

        }
    }
}
