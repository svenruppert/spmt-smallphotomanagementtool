package org.rapidpm.spmt.crawler.filesystem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.rapidpm.commons.cdi.logger.CDILogger;
import org.rapidpm.module.se.commons.logger.Logger;
import org.rapidpm.spmt.crawler.Crawler;
import org.rapidpm.spmt.crawler.CrawlerType;
import org.rapidpm.spmt.crawler.CrawlerTypeEnum;
import org.rapidpm.spmt.crawler.filesystem.extractor.FileExtractor;
import org.rapidpm.spmt.data.Node;

/**
 * Created with IntelliJ IDEA.
 * User: rapidpm
 * Date: 24.01.13
 * Time: 14:14
 * To change this template use File | Settings | File Templates.
 */

@CrawlerType(CrawlerTypeEnum.FILE_SYSTEM)
public class FileSystemCrawler implements Crawler<String> {


    @Inject @CDILogger private Logger logger;
    @Inject @Default   private FileExtractorRegistry registry;

    private List<String> uriList = new ArrayList<>();

    private Node rootNode;

    @Override
    public void crawl(){
        for (final String uri : uriList) {
            final File startPoint = new File(uri);
            crawlNextLevel(rootNode, startPoint);
        }
    }

    private void crawlNextLevel(Node rootNode, File startPoint) {
        if (startPoint.exists()){
            final Node nextNode = new Node();
            final boolean isFile = startPoint.isFile();
            if (isFile){
                //kein rekursiver Abstieg
                if (logger.isDebugEnabled()) {
                    logger.debug("startPoint is a File " + startPoint);
                }
            } else {
                final File[] files = startPoint.listFiles();
                if(files != null){
                    for (final File nextFile : files) {
                        crawlNextLevel(nextNode, nextFile);
                    }
                } else{
                    if (logger.isDebugEnabled()) {
                        logger.debug("files = startPoint.listFiles(); == null");
                    }
                }
            }
            //extractMetaData(file);
            final List<FileExtractor> extractorList = registry.getExtractorList();
            for (final FileExtractor fileExtractor : extractorList) {
                fileExtractor.extract(startPoint, nextNode);
            }
            rootNode.getNodeList().add(nextNode);

        } else {
            //TODO logger - kein StartPunkt
        }
    }

    @Override
    public void addRoodNode(Node rootNode) {
       this.rootNode = rootNode;
    }

    @Override
    public void addStartURL(@NotNull String uri) {
        this.uriList.add(uri);
    }
}
