package org.rapidpm.spmt.crawler.filesystem.extractor;

import java.io.File;

import javax.inject.Inject;

import org.rapidpm.commons.cdi.logger.CDILogger;
import org.rapidpm.spmt.data.Node;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 21.05.13
 * Time: 20:30
 * To change this template use File | Settings | File Templates.
 */
public class FileNameExtractor implements FileExtractor {


    public static final String FILE_NAME = "FILE_NAME";
    public static final String FILE_PATH = "FILE_PATH";

    @Inject @CDILogger
    private org.rapidpm.module.se.commons.logger.Logger logger;



    @Override
    public void extract(File file, Node node) {
        final String name = file.getName();
        node.setFile(file.isFile());
        node.getAttributMap().put(FILE_NAME, name);
        if (logger.isDebugEnabled()) {
            logger.debug("extracted " + name);
        }

        final String parent = file.getParent();
        node.getAttributMap().put(FILE_PATH, parent);
        if (logger.isDebugEnabled()) {
            logger.debug("extracted " + parent);
        }
    }
}
