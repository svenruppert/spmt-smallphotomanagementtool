package org.rapidpm.spmt.crawler.filesystem.extractor;

import org.rapidpm.spmt.data.Node;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: rapidpm
 * Date: 24.01.13
 * Time: 16:27
 * To change this template use File | Settings | File Templates.
 */
public interface FileExtractor {
    String FILE_SIZE = "FILE_SIZE";

    public void extract(File file, Node node);
}
