package org.rapidpm.spmt.crawler.action;

import org.rapidpm.spmt.data.Node;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 24.05.13
 * Time: 21:24
 * To change this template use File | Settings | File Templates.
 */
public interface Action {

    public void workOnNode(final Node node);



}
