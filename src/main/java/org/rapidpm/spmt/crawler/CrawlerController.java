package org.rapidpm.spmt.crawler;

import javax.inject.Inject;

import org.rapidpm.commons.cdi.logger.CDILogger;
import org.rapidpm.module.se.commons.logger.Logger;
import org.rapidpm.spmt.crawler.filesystem.FileSystemCrawler;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 22.05.13
 * Time: 21:34
 * To change this template use File | Settings | File Templates.
 */
public class CrawlerController {

    @Inject @CDILogger private Logger logger;


    @Inject @CrawlerType(CrawlerTypeEnum.FILE_SYSTEM) private FileSystemCrawler fsCrawler;

    public Crawler getFsCrawler(){
        return fsCrawler;
    }



}
