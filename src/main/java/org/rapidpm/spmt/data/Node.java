package org.rapidpm.spmt.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: rapidpm
 * Date: 24.01.13
 * Time: 14:08
 * To change this template use File | Settings | File Templates.
 */
public class Node {

    private Map<String, String> attributMap = new HashMap<>();

    private List<Node> nodeList = new ArrayList<>();

    private boolean file = false;



    @Override
    public String toString() {
        return "Node{" +
                ", attributMap=" + attributMap +
                ", nodeList=" + nodeList +
                '}';
    }

    public List<Node> getNodeList() {
        return nodeList;
    }

    public void setNodeList(List<Node> nodeList) {
        this.nodeList = nodeList;
    }

    public Map<String, String> getAttributMap() {
        return attributMap;
    }

    public boolean isFile() {
        return file;
    }

    public void setFile(boolean file) {
        this.file = file;
    }

    public static class Attribute {
        private String key;
        private String value;

        @Override
        public String toString() {
            return "Attribute{" +
                    "key='" + key + '\'' +
                    ", value='" + value + '\'' +
                    '}';
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

}
