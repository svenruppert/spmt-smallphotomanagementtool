package org.rapidpm.spmt.data;

import org.rapidpm.commons.cdi.contextresolver.CDIContext;

/**
 * Created by Sven Ruppert on 21.12.13.
 */
public class Context implements CDIContext{
    @Override
    public boolean isMockedModusActive() {
        return false;
    }

}
