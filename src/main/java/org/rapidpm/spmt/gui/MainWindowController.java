package org.rapidpm.spmt.gui;


import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import org.gephi.data.attributes.api.AttributeColumn;
import org.gephi.data.attributes.api.AttributeController;
import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.filters.api.FilterController;
import org.gephi.filters.api.Query;
import org.gephi.filters.api.Range;
import org.gephi.filters.plugin.graph.DegreeRangeBuilder;
import org.gephi.graph.api.*;
import org.gephi.io.exporter.api.ExportController;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.EdgeDefault;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.layout.plugin.force.StepDisplacement;
import org.gephi.layout.plugin.force.yifanHu.YifanHuLayout;
import org.gephi.preview.ProcessingRenderTargetBuilder;
import org.gephi.preview.api.*;
import org.gephi.preview.plugin.items.NodeItem;
import org.gephi.preview.spi.RenderTargetBuilder;
import org.gephi.preview.types.EdgeColor;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.gephi.ranking.api.Ranking;
import org.gephi.ranking.api.RankingController;
import org.gephi.ranking.api.Transformer;
import org.gephi.ranking.plugin.transformer.AbstractColorTransformer;
import org.gephi.ranking.plugin.transformer.AbstractSizeTransformer;
import org.gephi.statistics.plugin.GraphDistance;
import org.openide.util.Lookup;
import org.rapidpm.commons.cdi.fx.CDIJavaFXBaseApp;
import org.rapidpm.commons.cdi.fx.CDIJavaFxBaseController;
import processing.core.PGraphics;
import processing.core.PGraphicsJava2D;

import javax.inject.Inject;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Sven Ruppert on 14.09.13.
 */
public class MainWindowController implements CDIJavaFxBaseController {

    @Inject @CDIJavaFXBaseApp
    Application.Parameters applicationParameters;

    @FXML AnchorPane middleWorkingAnchorPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        System.out.println("url = " + url);
//        calculateGephiDemo();
//        calculateDemoGraph();
    }


    public void onClicked(){
        calculateDemoGraph();

    }


    private void calculateDemoGraph(){
        ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
        pc.newProject();
        Workspace workspace = pc.getCurrentWorkspace();

//Get a graph model - it exists because we have a workspace
        GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
        AttributeModel attributeModel = Lookup.getDefault().lookup(AttributeController.class).getModel();
        RankingController rankingController = Lookup.getDefault().lookup(RankingController.class);
        PreviewController previewController = Lookup.getDefault().lookup(PreviewController.class);

//Create three nodes
        Node n0 = graphModel.factory().newNode("n0");
        n0.getNodeData().setLabel("Node 0");
        Node n1 = graphModel.factory().newNode("n1");
        n1.getNodeData().setLabel("Node 1");
        Node n2 = graphModel.factory().newNode("n2");
        n2.getNodeData().setLabel("Node 2");

//Create three edges
        Edge e1 = graphModel.factory().newEdge(n1, n2, 1f, true);
        Edge e2 = graphModel.factory().newEdge(n0, n2, 2f, true);
        Edge e3 = graphModel.factory().newEdge(n2, n0, 2f, true);   //This is e2's mutual edge

//Append as a Directed Graph
        DirectedGraph directedGraph = graphModel.getDirectedGraph();
        directedGraph.addNode(n0);
        directedGraph.addNode(n1);
        directedGraph.addNode(n2);
        directedGraph.addEdge(e1);
        directedGraph.addEdge(e2);
        directedGraph.addEdge(e3);

        //Run YifanHuLayout for 100 passes - The layout always takes the current visible view
        YifanHuLayout layout = new YifanHuLayout(null, new StepDisplacement(1f));
        layout.setGraphModel(graphModel);
        layout.resetPropertiesValues();
        layout.setOptimalDistance(200f);
        layout.initAlgo();

        for (int i = 0; i < 100 && layout.canAlgo(); i++) {
            layout.goAlgo();
        }
        layout.endAlgo();

////Get Centrality
        final GraphDistance distance = new GraphDistance();
        distance.setDirected(true);
        distance.execute(graphModel, attributeModel);
//
////Rank color by Degree
        Ranking degreeRanking = rankingController.getModel().getRanking(Ranking.NODE_ELEMENT, Ranking.DEGREE_RANKING);
        AbstractColorTransformer colorTransformer = (AbstractColorTransformer) rankingController.getModel().getTransformer(Ranking.NODE_ELEMENT, Transformer.RENDERABLE_COLOR);
        colorTransformer.setColors(new Color[]{new Color(0xFEF0D9), new Color(0xB30000)});
        rankingController.transform(degreeRanking,colorTransformer);
//
////Rank size by centrality
        AttributeColumn centralityColumn = attributeModel.getNodeTable().getColumn(GraphDistance.BETWEENNESS);
        Ranking centralityRanking = rankingController.getModel().getRanking(Ranking.NODE_ELEMENT, centralityColumn.getId());
        AbstractSizeTransformer sizeTransformer = (AbstractSizeTransformer) rankingController.getModel().getTransformer(Ranking.NODE_ELEMENT, Transformer.RENDERABLE_SIZE);
        sizeTransformer.setMinSize(3);
        sizeTransformer.setMaxSize(10);
        rankingController.transform(centralityRanking,sizeTransformer);
//
////Preview
//        PreviewProperties props = model.getProperties();
//        props.putValue("width", 800);
//        props.putValue("height", 600);
        PreviewModel model = previewController.getModel();

        final int width = (int) middleWorkingAnchorPane.getWidth();
        final int height = (int) middleWorkingAnchorPane.getHeight();
        final Point2D point2D = middleWorkingAnchorPane.localToScene(0.0, 0.0);
        final double layoutX = point2D.getX();
        final double layoutY = point2D.getY();
        System.out.println("layoutX = " + layoutX);
        System.out.println("layoutY = " + layoutY);

        System.out.println("height = " + height);
        System.out.println("width = " + width);

        model.getProperties().putValue("width", width);
        model.getProperties().putValue("height", height);
        model.getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.TRUE);
        model.getProperties().putValue(PreviewProperty.EDGE_COLOR, new EdgeColor(Color.GRAY));
        model.getProperties().putValue(PreviewProperty.EDGE_THICKNESS, new Float(0.1f));
        model.getProperties().putValue(PreviewProperty.NODE_LABEL_FONT, model.getProperties().getFontValue(PreviewProperty.NODE_LABEL_FONT).deriveFont(8));


        RenderTargetBuilder rtb = new ProcessingRenderTargetBuilder();
        final RenderTarget renderTarget = rtb.buildRenderTarget(model);
        previewController.render(renderTarget);

        ProcessingTarget target = (ProcessingTarget)previewController.getRenderTarget(RenderTarget.PROCESSING_TARGET);
        final PGraphics targetGraphics = target.getGraphics();
        Graphics2D targetGraphics2D = ((PGraphicsJava2D)targetGraphics).g2;

        previewController.refreshPreview();
        final Dimension dimensions = model.getDimensions();
        System.out.println("dimensions = " + dimensions);
        final Item[] itemsNode = model.getItems(Item.NODE);
        for (final Item item : itemsNode) {
            final float x = item.getData("x");
            final float y = item.getData("y");
            final float size = item.getData("size");

            Circle circle = new Circle(x+layoutX, y+layoutY, size);

            Color color = item.getData("color");
            circle.setStroke(javafx.scene.paint.Color.rgb(color.getRed(), color.getGreen(), color.getBlue()));
            circle.setFill(javafx.scene.paint.Color.rgb(color.getRed(), color.getGreen(), color.getBlue()));

            circle.setOnMouseEntered(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent me) {
                    System.out.println("Mouse entered");
                }
            });
            circle.setOnMouseExited(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent me) {
                    System.out.println("Mouse exited");
                }
            });
            circle.setOnMousePressed(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent me) {
                    System.out.println("Mouse pressed");
                }
            });

            makeDraggable(circle);
            middleWorkingAnchorPane.getChildren().addAll(makeDraggable(circle));

        }

        final Item[] itemsEdge = model.getItems(Item.EDGE);
        for (final Item item : itemsEdge) {
            System.out.println("item = " + item);
            final Float weight = item.getData("weight");

            final NodeItem sourceNode = item.getData("source");
            final float sourceX = sourceNode.getData("x");
            final float sourceY = sourceNode.getData("y");
            System.out.println("sourceX = " + sourceX+layoutX);
            System.out.println("sourceY = " + sourceY+layoutY);

            final NodeItem targetNode = item.getData("target");
            final float targetX = targetNode.getData("x");
            final float targetY = targetNode.getData("y");

            System.out.println("targetX = " + targetX+layoutX);
            System.out.println("targetY = " + targetY+layoutY);

            final Line line = new Line();
            line.setStartX(sourceX+layoutX);
            line.setStartY(sourceY+layoutY);
            line.setEndX(targetX+layoutX);
            line.setEndY(targetY+layoutY);
            line.setFill(javafx.scene.paint.Color.BLACK);
            line.setStroke(javafx.scene.paint.Color.BLACK);

            middleWorkingAnchorPane.getChildren().addAll(line);
        }



        System.out.println("middleWorkingAnchorPane = " + middleWorkingAnchorPane);



    }


    private Circle makeDraggable(final Circle node) {
        final DragContext dragContext = new DragContext();
//        final Group wrapGroup = new Group(node);

        node.addEventFilter(
                MouseEvent.ANY,
                new EventHandler<MouseEvent>() {
                    public void handle(final MouseEvent mouseEvent) {
                            mouseEvent.consume();
//                        if (dragModeActiveProperty.get()) {
//                            disable mouse events for all children
//                        }
                    }
                });

        node.addEventFilter(
                MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    public void handle(final MouseEvent mouseEvent) {
                            // remember initial mouse cursor coordinates
                            // and node position
                            dragContext.mouseAnchorX = mouseEvent.getX();
                            dragContext.mouseAnchorY = mouseEvent.getY();
                            dragContext.initialTranslateX = node.getTranslateX();
                            dragContext.initialTranslateY = node.getTranslateY();
//                        if (dragModeActiveProperty.get()) {
//                        }
                    }
                });

        node.addEventFilter(
                MouseEvent.MOUSE_DRAGGED,
                new EventHandler<MouseEvent>() {
                    public void handle(final MouseEvent mouseEvent) {
                            // shift node from its initial position by delta
                            // calculated from mouse cursor movement
//                            node.setTranslateX( dragContext.initialTranslateX + mouseEvent.getX() - dragContext.mouseAnchorX);
                            node.setCenterX(mouseEvent.getX());
//                            node.setTranslateY( dragContext.initialTranslateY + mouseEvent.getY() - dragContext.mouseAnchorY);
                            node.setCenterY(mouseEvent.getY());
//                        if (dragModeActiveProperty.get()) {
//                        }
                    }
                });

        return node;

    }

    private static final class DragContext {
        public double mouseAnchorX;
        public double mouseAnchorY;
        public double initialTranslateX;
        public double initialTranslateY;
    }


    private void calculateGephiDemo(){
        //Init a project - and therefore a workspace
        ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
        pc.newProject();
        Workspace workspace = pc.getCurrentWorkspace();

//Get models and controllers for this new workspace - will be useful later
        AttributeModel attributeModel = Lookup.getDefault().lookup(AttributeController.class).getModel();
        GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
        PreviewModel model = Lookup.getDefault().lookup(PreviewController.class).getModel();
        ImportController importController = Lookup.getDefault().lookup(ImportController.class);
        FilterController filterController = Lookup.getDefault().lookup(FilterController.class);
        RankingController rankingController = Lookup.getDefault().lookup(RankingController.class);

//Import file
        Container container;
        try {
            File file = new File(getClass().getResource("polblogs.gml").toURI());
            container = importController.importFile(file);
            container.getLoader().setEdgeDefault(EdgeDefault.DIRECTED);   //Force DIRECTED
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }

//Append imported data to GraphAPI
        importController.process(container, new DefaultProcessor(), workspace);

//See if graph is well imported
        DirectedGraph graph = graphModel.getDirectedGraph();
        System.out.println("Nodes: " + graph.getNodeCount());
        System.out.println("Edges: " + graph.getEdgeCount());

//Filter
        DegreeRangeBuilder.DegreeRangeFilter degreeFilter = new DegreeRangeBuilder.DegreeRangeFilter();
        degreeFilter.init(graph);
        degreeFilter.setRange(new Range(30, Integer.MAX_VALUE));     //Remove nodes with degree < 30
        Query query = filterController.createQuery(degreeFilter);

//        GraphView view = filterController.filter(query);
//        graphModel.setVisibleView(view);    //Set the filter result as the visible view

//See visible graph stats
        UndirectedGraph graphVisible = graphModel.getUndirectedGraphVisible();
        System.out.println("Nodes: " + graphVisible.getNodeCount());
        System.out.println("Edges: " + graphVisible.getEdgeCount());

//Run YifanHuLayout for 100 passes - The layout always takes the current visible view
        YifanHuLayout layout = new YifanHuLayout(null, new StepDisplacement(1f));
        layout.setGraphModel(graphModel);
        layout.resetPropertiesValues();
        layout.setOptimalDistance(200f);
        layout.initAlgo();

        for (int i = 0; i < 100 && layout.canAlgo(); i++) {
            layout.goAlgo();
        }
        layout.endAlgo();

//Get Centrality
        final GraphDistance distance = new GraphDistance();
        distance.setDirected(true);
        distance.execute(graphModel, attributeModel);

//Rank color by Degree
        Ranking degreeRanking = rankingController.getModel().getRanking(Ranking.NODE_ELEMENT, Ranking.DEGREE_RANKING);
        AbstractColorTransformer colorTransformer = (AbstractColorTransformer) rankingController.getModel().getTransformer(Ranking.NODE_ELEMENT, Transformer.RENDERABLE_COLOR);
        colorTransformer.setColors(new Color[]{new Color(0xFEF0D9), new Color(0xB30000)});
        rankingController.transform(degreeRanking,colorTransformer);

//Rank size by centrality
        AttributeColumn centralityColumn = attributeModel.getNodeTable().getColumn(GraphDistance.BETWEENNESS);
        Ranking centralityRanking = rankingController.getModel().getRanking(Ranking.NODE_ELEMENT, centralityColumn.getId());
        AbstractSizeTransformer sizeTransformer = (AbstractSizeTransformer) rankingController.getModel().getTransformer(Ranking.NODE_ELEMENT, Transformer.RENDERABLE_SIZE);
        sizeTransformer.setMinSize(3);
        sizeTransformer.setMaxSize(10);
        rankingController.transform(centralityRanking,sizeTransformer);

//Preview
        model.getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.TRUE);
        model.getProperties().putValue(PreviewProperty.EDGE_COLOR, new EdgeColor(Color.GRAY));
        model.getProperties().putValue(PreviewProperty.EDGE_THICKNESS, new Float(0.1f));
        model.getProperties().putValue(PreviewProperty.NODE_LABEL_FONT, model.getProperties().getFontValue(PreviewProperty.NODE_LABEL_FONT).deriveFont(8));

        final NodeIterable nodes = graphVisible.getNodes();
        for (final Node node : nodes) {
            final float x = node.getNodeData().x();
            final float y = node.getNodeData().y();
            final float z = node.getNodeData().z();
            System.out.println("node = " + node);


        }

//Export
        ExportController ec = Lookup.getDefault().lookup(ExportController.class);
        try {
            ec.exportFile(new File("headless_simple.pdf"));
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }
    }


}
