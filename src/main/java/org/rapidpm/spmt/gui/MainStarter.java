package org.rapidpm.spmt.gui;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import javafx.scene.Scene;
import javafx.stage.Stage;
import org.rapidpm.commons.cdi.fx.CDIStartupScene;

/**
 * Created by Sven Ruppert on 14.09.13.
 */
public class MainStarter {

    @Inject MainWindow mainWindow;

    public void launchJavaFXApplication(@Observes @CDIStartupScene Stage stage) {

        System.out.println("stage = " + stage);
        stage.setScene(new Scene(mainWindow));
        stage.show();
    }

}
