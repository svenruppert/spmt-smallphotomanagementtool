package org.rapidpm.spmt.gui;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 24.05.13
 * Time: 22:32
 * To change this template use File | Settings | File Templates.
 */

import javafx.stage.Stage;
import org.rapidpm.commons.cdi.fx.CDIJavaFXBaseApplication;

public class Main extends CDIJavaFXBaseApplication {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void startImpl(Stage primaryStage) throws Exception {
        //nothing to do now
    }
}
