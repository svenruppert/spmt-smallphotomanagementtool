package org.rapidpm.spmt.gui;

import javax.inject.Inject;

import javafx.scene.control.Button;
import org.rapidpm.commons.cdi.fx.components.CDIBaseAnchorPane;

/**
 * Created by Sven Ruppert on 14.09.13.
 */
public class MainWindow extends CDIBaseAnchorPane<MainWindow, MainWindowController> {


    public Button printGraph;
    @Inject MainWindowController controller;

    @Override
    public Class<MainWindow> getPaneClass() {
        return MainWindow.class;
    }

    @Override
    public MainWindowController getController() {
        return controller;
    }

    @Override
    public void setController(MainWindowController controller) {
        this.controller = controller;
    }
}
