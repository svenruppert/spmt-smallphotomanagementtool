package org.rapidpm.spmt.gui;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.JFileChooser;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import org.apache.log4j.Logger;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.rapidpm.spmt.crawler.CrawlerController;
import org.rapidpm.spmt.data.Node;

/**
 * Created with IntelliJ IDEA.
 * User: Sven Ruppert
 * Date: 24.05.13
 * Time: 22:33
 * To change this template use File | Settings | File Templates.
 */
public class MainController  implements Initializable{


    public Label crawlerTabLabelStartPoint;
    private final WeldContainer weldContainer = new Weld().initialize();  //TODO hier nicht
    private final Logger logger = weldContainer.instance().select(Logger.class).get();
    private final CrawlerController crawlerController = weldContainer.instance().select(CrawlerController.class).get();
    public TreeView<NodeFormatter> crawlerTabTreeView;
    public TextArea crawlerTabTextField;


    public void onClickedSelectStartPoint(ActionEvent actionEvent) {
        final JFileChooser fileChooser = new JFileChooser(".");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        final int ret = fileChooser.showOpenDialog(null);
        if(ret == JFileChooser.APPROVE_OPTION){
            final File selectedFile = fileChooser.getSelectedFile();
            crawlerTabLabelStartPoint.setText(selectedFile.getPath());
        } else{
            if (logger.isDebugEnabled()) {
                logger.debug("FileChooser != JFileChooser.APPROVE_OPTION");
            }
        }
    }

   public void onMouseClickedTreeView(MouseEvent event) {
       System.out.println("actionEvent = " + event);
       if (logger.isDebugEnabled()) {
           logger.debug("onMouseClickedTreeView - " + event);
       }
       final TreeItem<NodeFormatter> selectedItem = crawlerTabTreeView.getSelectionModel().getSelectedItem();
       if(selectedItem != null){
           final NodeFormatter value = selectedItem.getValue();
           final Node node = value.getNode();
           if(node == null){
               crawlerTabTextField.setText("");
           } else{
               final Map<String,String> attributMap = node.getAttributMap();
               final StringBuilder sb = new StringBuilder();
               for (final Map.Entry<String, String> entry : attributMap.entrySet()) {
                   sb.append(entry.toString()).append('\n');
               }
               crawlerTabTextField.setText(sb.toString());
           }
       } else{
           if (logger.isDebugEnabled()) {
               logger.debug("selectedItem == null");
           }
       }
   }

    public void onGoClicked(ActionEvent actionEvent) {
        if (crawlerTabLabelStartPoint.getText() == null || crawlerTabLabelStartPoint.getText().isEmpty()  ) {
            //erst Pfad auswählen
        } else {
            //Hole CrawlerMainController
//            final Node rootNode = new Node();
//
//            final Crawler crawler = crawlerController.getFsCrawler();
//            crawler.crawl(rootNode, crawlerTabLabelStartPoint.getText());
//
//            final TreeItem<NodeFormatter> rootTreeItem = new TreeItem<>(new NodeFormatter(rootNode));
//            rootTreeItem.setExpanded(true);
//
//            nextLevelOfNodes(rootNode, rootTreeItem);
//            crawlerTabTreeView.setRoot(rootTreeItem);


        }
    }

    private void nextLevelOfNodes(Node rootNode, TreeItem<NodeFormatter> rootTreeItem) {
        final List<Node> nodeList = rootNode.getNodeList();
        for (final Node node : nodeList) {
            final TreeItem<NodeFormatter> nextItem = new TreeItem<>(new NodeFormatter(node));
            rootTreeItem.getChildren().add(nextItem);
            nextLevelOfNodes(node, nextItem);
        }
    }

    private class NodeFormatter {
        private final Node node ;

        private NodeFormatter(Node node) {
            this.node = node;
        }

        private Node getNode() {
            return node;
        }

        @Override
        public String toString() {
//            final List<Node.Attribute> attributList = node.getAttributList();
//            for (final Node.Attribute attribute : attributList) {
//                if (attribute.getKey().equals(FileNameExtractor.FILE_NAME)) {
//                    return attribute.getValue();
//                } else {
//                    if (logger.isDebugEnabled()) {
//                        logger.debug("falsche Attribut..");
//                    }
//                }
//            }
            return "##ERROR##";
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        crawlerTabLabelStartPoint.setText(".");
        crawlerTabTreeView.setShowRoot(false);
    }
}
